<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Perintah Lembur <small><a href="tabel_surat_undangan_rapat.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
					
					if(isset($_POST['update'])){
            
            // undangan
           
            $undangan='';
            foreach($_POST['undangan'] as $value){

              if(!empty($value)){

              

                $undangan .=$value.'&';

                

              }
            }

            $undangan=substr($undangan,0,strlen($undangan)-1);

            // end undangan

            // agenda
          
            $agenda='';
            foreach($_POST['agenda'] as $value){

              if(!empty($value)){

              

                $agenda .=$value.'&';

                

              }
            }

            $agenda=substr($agenda,0,strlen($agenda)-1);

            // end agenda

            
						
						$up = mysqli_query($link, "UPDATE surat_undangan_rapat SET 
							
              
							kepada='$_POST[kepada]',
              hari='$_POST[hari]',
              tanggal='$_POST[tanggal]',
              waktu='$_POST[waktu]',
              tempat='$_POST[tempat]',
							undangan='$undangan',
							agenda_rapat='$agenda'
							
              where id='$_GET[id]'
						");
						
						if($up){
              
              //for cetak

              // $html=file_get_contents("template/suratketerangan.php",$data);
              // $pdf=mpdf($html);


              echo mysqli_error($up);

              // if($pdf==true){
                header("Location: form_surat_undangan_rapat.php?ID=".$_POST['no_surat']."&&sukses=ya");
              // }
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Di Update !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_surat_undangan_rapat.php');
								  } ,1000); 
								</script>";
					}
					
					?>
          <?php           
          
            $query=mysqli_query($link,"SELECT * FROM surat_undangan_rapat WHERE id='$_GET[id]'");
            $rows=mysqli_fetch_object($query);

            $undangan=membuatlistsingle($rows->undangan);
            $agenda=membuatlistsingle($rows->agenda_rapat);
            
          
          ?>

                    <form method="post" name="surat_undangan_rapat" id="surat_undangan_rapat" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">

                     

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kepada" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->kepada?>'>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hari <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="hari" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->hari?>'>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="tanggal" required="required" class="date-picker form-control col-md-7 col-xs-12" value='<?php echo $rows->tanggal?>'>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Waktu <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="waktu" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->waktu?>'>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tempat <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="tempat" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->tempat?>'>
                        </div>
                      </div>


                      <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Undangan<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php foreach($undangan as $value){?>

                        <div class="input-group control-group <?php if($value['no']==1){?>after-undangan<?php }?>">
                        
                          
                          <div class='row'>
                              <div class='col-lg-12'>
                              <input type="text" name="undangan[]" class="form-control" placeholder="Jabatan" value='<?php echo $value['nama']?>'>
                              </div>
                            
                              
                          </div>
                          <?php if($value['no']==1){?>
                          <div class="input-group-btn"> 
                            <button class="btn btn-success add-more-undangan" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                          </div>
                          <?php }else{?>
                            <div class="input-group-btn"> 
                              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                          <?php }?>
                        </div>

                      <!-- tambah disini -->
                         <?php }?>
                        
                        </div>
                      </div>


                     
                      <div class="copy-undangan hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="undangan[]" class="form-control" placeholder="Nama">
                            </div>

                           
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-undangan").click(function(){ 
                              var html = $(".copy-undangan").html();
                              $(".after-undangan").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->


                    <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Agenda<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php foreach($agenda as $value){?>

                        <div class="input-group control-group <?php if($value['no']==1){?>after-agenda<?php }?>">
                        
                          
                          <div class='row'>
                              <div class='col-lg-12'>
                              <input type="text" name="agenda[]" class="form-control" placeholder="Jabatan" value='<?php echo $value['nama']?>'>
                              </div>
                            
                              
                          </div>
                          <?php if($value['no']==1){?>
                          <div class="input-group-btn"> 
                            <button class="btn btn-success add-more-agenda" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                          </div>
                          <?php }else{?>
                            <div class="input-group-btn"> 
                              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                          <?php }?>
                        </div>

                      <!-- tambah disini -->
                         <?php }?>
                        
                        </div>
                      </div>


                     
                      <div class="copy-agenda hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="agenda[]" class="form-control" placeholder="Nama">
                            </div>

                           
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-agenda").click(function(){ 
                              var html = $(".copy-agenda").html();
                              $(".after-agenda").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->

                    
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button name='update' type="submit" class="btn btn-success">Update</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>