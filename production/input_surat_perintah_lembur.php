<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Tugas <small><a href="tabel_surat_perintah_lembur.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
					
					if(isset($_POST['input'])){
            

             // nama
           
             $nama='';
             foreach($_POST['nama'] as $value){
 
               if(!empty($value)){
 
               
 
                 $nama .=$value.'&';
 
                 
 
               }
             }
 
             $nama=substr($nama,0,strlen($nama)-1);
 
             // end nama

            

            $date=date('Y-m-d');
            


            // mencari no surat
            $query = "SELECT max(no_surat) as maxKode FROM surat_perintah_lembur";
            $hasil = mysqli_query($link,$query);
            $data = mysqli_fetch_array($hasil);
            $kode = $data['maxKode'];
          
            if(!empty($kode)){

              $noUrut = (int) substr($kode, 0, 3);
            
              $noUrut++;
              
              $char = "/ST-Rek/DPPAI/VIII/2018";
              $kode = sprintf("%03s", $noUrut).$char;
              

            }else{

              $kode = '001/ST-Rek/DPPAI/VIII/2018';
            }
          
            
            $no_surat=$kode;

            //selesai cari no surat

						$up = mysqli_query($link, "INSERT INTO surat_perintah_lembur VALUES 
							('',
            	'$no_surat',
            	'$nama',
            	'$_POST[tugas]',
            	'$_POST[kota]',
							'$_POST[tanggal_m]',
							'$date'
							)
						
						");
						
						if($up){
              
              
              header("Location: form_surat_perintah_lembur.php?ID=".$_POST['no_surat']."&&sukses=ya");
             
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Diarsipkan !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_surat_perintah_lembur.php');
								  } ,1000); 
								</script>";
					}
					
					?>
                    <form method="post" name="surat_perintah_lembur" id="surat_perintah_lembur" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">

                     

                      <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-nama">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="nama[]" class="form-control" placeholder="nama">
                            </div>

                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-nama" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-nama hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="nama[]" class="form-control" placeholder="Nama">
                            </div>

                           
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-nama").click(function(){ 
                              var html = $(".copy-nama").html();
                              $(".after-nama").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->

                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tugas <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control" rows="5" name="tugas"></textarea>
                        </div>
                      </div>

                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kota" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

										
											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Masehi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="tanggal_m" required="required" class="date-picker form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Atur Ulang</button>
                          <button name='input' type="submit" class="btn btn-success">Buat Surat</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>