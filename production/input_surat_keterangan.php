<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Masuk <small><a href="tabel_surat_keterangan.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
          
          

					if(isset($_POST['input'])){
            
             // membuat no surat

            // mencari no surat
            $query = "SELECT max(no_surat) as maxKode FROM surat_keterangan";
            $hasil = mysqli_query($link,$query);
            $data = mysqli_fetch_array($hasil);
            $kode = $data['maxKode'];
          
            if(!empty($kode)){

              $noUrut = (int) substr($kode, 0, 3);
            
              $noUrut++;
              
              $char = "/Dir/10/DPPAI/IX/2018";
              $kode = sprintf("%03s", $noUrut).$char;
              

            }else{

              $kode = '001/Dir/10/DPPAI/IX/2018';
            }
          
            
            $no_surat=$kode;

            //selesai cari no surat

						$date=date('Y-m-d');
						$up = mysqli_query($link, "INSERT INTO surat_keterangan VALUES 
							('',
							'$no_surat',
							'$_POST[nim]',
							'$_POST[nama]',
							'$_POST[jurusan]',
							'$_POST[fakultas]',
							'$_POST[mengikuti]',
							'$_POST[nilai]',
              '$_POST[kota]',
							'$_POST[tanggal_m]',
							'$date'
							)
						
						");
						
						if($up){
              
              //for cetak

              // $html=file_get_contents("template/suratketerangan.php",$data);
              // $pdf=mpdf($html);



              // if($pdf==true){
                header("Location: form_surat_keterangan.php?ID=".$_POST['no_surat']."&&sukses=ya");
              // }
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Diarsipkan !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_surat_keterangan.php');
								  } ,1000); 
								</script>";
					}
					
					?>
                    <form method="post" name="surat_keterangan" id="surat_keterangan" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NIM <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nim" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jurusan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="jurusan" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fakultas <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="fakultas" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mengikuti <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control" rows="5" name="mengikuti"></textarea>
                        </div>
                      </div>

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nilai <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nilai" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kota" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

		
											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="tanggal_m" required="required" class="date-picker form-control col-md-7 col-xs-12">
                        </div>
                  
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Atur Ulang</button>
                          <button name='input' type="submit" class="btn btn-success">Buat Surat</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>