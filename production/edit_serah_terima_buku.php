<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Masuk <small><a href="tabel_serah_terima_buku.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
					
					if(isset($_POST['update'])){
            print_r($_POST);
            // buku
            $i=true;
            $buku='';
            foreach($_POST['buku'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $buku .=$value.'-';
                  
                  $i=false;
                }else{

                  $buku .=$value.'&';

                  $i=true;
                }

              }
            }

            $buku=substr($buku,0,strlen($buku)-1);

            // end penanggung jawab

            // yang_menyerahkan
            $i=true;
            $yang_menyerahkan='';
            foreach($_POST['yang_menyerahkan'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $yang_menyerahkan .=$value.'-';
                  
                  $i=false;
                }else{

                  $yang_menyerahkan .=$value.'&';

                  $i=true;
                }

              }
            }

            $yang_menyerahkan=substr($yang_menyerahkan,0,strlen($yang_menyerahkan)-1);

            // end penanggung jawab
            
						
						$up = mysqli_query($link, "UPDATE serah_terima_buku SET 
							
              diberikan_untuk='$_POST[no_surat]',
              lokasi='$_POST[lokasi]',
              persetujuan='$_POST[persetujuan]',



							buku='$buku',
							yang_menyerahkan='$yang_menyerahkan'
							
							
              where id='$_GET[id]'
						");
						
						if($up){
              
              //for cetak

              // $html=file_get_contents("template/suratketerangan.php",$data);
              // $pdf=mpdf($html);


              echo mysqli_error($up);

              // if($pdf==true){
                header("Location: form_serah_terima_buku.php?ID=".$_POST['no_surat']."&&sukses=ya");
              // }
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Di Update !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_serah_terima_buku.php');
								  } ,1000); 
								</script>";
					}
					
					?>
          <?php           
          
            $query=mysqli_query($link,"SELECT * FROM serah_terima_buku WHERE id='$_GET[id]'");
            $rows=mysqli_fetch_object($query);

            $buku=membuatlist($rows->buku);
            $yang_menyerahkan=membuatlist($rows->yang_menyerahkan);
            
          
          ?>

                    <form method="post" name="serah_terima_buku" id="serah_terima_buku" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Di Berikan Untuk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="diberikan_untuk" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->diberikan_untuk?>'>
                        </div>
                      </div>


                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Lokasi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="lokasi" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->lokasi?>'>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Persetujuan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="persetujuan" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->persetujuan?>'>
                        </div>
                      </div>

                      <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penanggung Jawab<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php foreach($buku as $value){?>

                        <div class="input-group control-group <?php if($value['no']==1){?>after-buku<?php }?>">
                        
                          
                          <div class='row'>
                              <div class='col-lg-6'>
                              <input type="text" name="buku[]" class="form-control" placeholder="Jabatan" value='<?php echo $value['jabatan']?>'>
                              </div>

                              <div class='col-lg-6'>
                              <input type="text" name="buku[]" class="form-control" placeholder="Nama" value='<?php echo $value['nama']?>'>
                              </div>
                          </div>
                          <?php if($value['no']==1){?>
                          <div class="input-group-btn"> 
                            <button class="btn btn-success add-more-buku" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                          </div>
                          <?php }else{?>
                            <div class="input-group-btn"> 
                              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                          <?php }?>
                        </div>

                      <!-- tambah disini -->
                         <?php }?>
                        
                        </div>
                      </div>


                     
                      <div class="copy-buku hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="buku[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="buku[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-buku").click(function(){ 
                              var html = $(".copy-buku").html();
                              $(".after-buku").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->

                    
                      
                      

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kota" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->kota?>'>
                        </div>
                      </div>
                     
                    
                     

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="tanggal" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->tanggal?>'>
                        </div>
                      </div>
                      
                      <?php foreach($yang_menyerahkan as $yang_menyerahkan){?>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Yang Menyrahkan<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="yang_menyerahkan[]" class="form-control" placeholder="Nama" value='<?php echo $yang_menyerahkan['nama']?>'>
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="yang_menyerahkan[]" class="form-control" placeholder="Jabatan" value='<?php echo $yang_menyerahkan['jabatan']?>'>
                            </div>
                        </div>
                        
                        <div class="input-group-btn"> 
                          
                        </div>
                      </div>
                        </div>
                      </div>
                      <?php }?>
                      

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penerima <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="penerima" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $rows->penerima?>'>
                        </div>
                      </div>

                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button name='update' type="submit" class="btn btn-success">Update</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>