<html>

    <head>
        <title>Lembar Disposisi</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-18{

                font-size:18pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            .logo {
            color: white;
            display: block;
            width: 40px;
            height: 50px;
            padding: 5px;
            border: 1px solid blue; 
            background-color: blue; 
            }
            .table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            .table th {
                text-align:left;
                border: solid white;
            }

            .table td {
                border: solid white;
                font-size:11pt;
                
            }
            .ttd{
                float:right;
                width:50%; 
            }
            .p {
                border-style: solid;
                border-color: black;
                border-width: 2px 2px 2px 2px;
                padding:10px 10px 10px 10px;
            }
        </style>
    </head>


    <body>
        <div class='logo'>
            <p class='font-10 justify'>logo</p>
        </div>
        
        <p class='font-10 justify'>Direktorat Pendidikan dan Pengembangan Agama Islam</p>
        <p class='font-14 justify'>Universitas</p>
        <br>
        
        <div class='p'>
        <p class='font-14 justify'>Disposisi Ka.Div:</p>
        <hr>
        <hr>
        <hr>
        <hr>
        <hr>
        <br>
        <br>
        <p class='font-14 justify'>Disposisi Direktur:</p>
        <hr>
        <hr>
        <hr>
        <hr>
        <hr></div>
        <br>
       
        <br>
        <p class='font-14 justify'>Catatan Staf Terkait______________________________________:</p>
        <hr>
        <hr>
        <hr>
        <hr>
        
        <br>
        <div class='ttd'>
        <p class='font-12 italic'>Sekretariat DPPAI</p>
        </div>

        

        
    </body>

</html>