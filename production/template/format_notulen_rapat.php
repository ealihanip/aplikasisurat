<html>

    <head>
        <title>Notulen Rapat</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-18{

                font-size:18pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            .logo {
            color: white;
            display: block;
            width: 30px;
            height: 30px;
            padding: 5px;
            border: 1px solid blue; 
            background-color: blue; 
            }

            .fm{
                color: black;
                width:90%; 
                margin:0 auto; 
            }

            table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            table th {
                text-align:justify;
                height: 50px;
                border-bottom: 1px solid #ddd;
                border: 1px solid black;
                padding: 15px;
                
            }

            table td {
                font-size:12pt;
                height: 10px;
                vertical-align: bottom;
                border-bottom: 20px solid white;
                border: 1px solid black;
                padding: 15px;
                
            }
            .ttd{
                float:right;
                width:50%;   
            }
        </style>
    </head>


    <body>
        
        

        <div class='fm'>
        <p class='font-10 right'>FM-U-AM-FSM-05/R0</p>
        </div>

        <p class='font-12 bold center'>NOTULEN RAPAT</p>
        <br>
        <div style="overflow-x:auto;">
        
        <table>
            <tr>
                <td>
                    Rapat Tanggal: <?php echo $row->tanggal_notulen?>
                </td>
                <td>
                    Nomor Undangan Rapat: <?php echo $row->no_undangan?>
                </td>
            </tr>
            <tr>
                <td>
                Pokok Bahasan: <?php echo $row->pokok_bahasan?>
                </td>
                <td>
                Halaman : <?php echo $row->halaman?>
                </td>
            </tr>
            </table>
            <table class='font-10 center'>
            <tr>
                <td>
                NO
                </td>
                <td>
                Rincian Pokok Bahasan
                </td>
                <td>
                Bahasan
                </td>
                <td>
                Tindak Lanjut
                </td>
            </tr>

            <?php $no=1; foreach($bahasan as $value){?>
            <tr>
                <td>
                <?php echo $no?>
                </td>
                    
                <td>
                    <?php echo $value['0']?>
                </td>
                    
                <td>
                    <?php echo $value['1']?>
                </td>
                    
                <td>
                <?php echo $value['0']?>
                </td>
            </tr>
            <?php $no++; }?>
        </table>
        <br>
        <br>
        
        <div class='ttd'>
        <p class='font-10 center'>Notulis,</p>
        <br>
        <br>
        <br>
        <br>
        <p class='font-10 center'>________________</p>
        <br>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p class='font-10 bold'>Undangan Yang Hadir:</p>
        <table class='font-10 center'>
       
            <tr>
                <td>
                    NO
                </td>
                <td>
                    NAMA
                </td>
                <td>
                    JABATAN
                </td>
                <td>
                    KETERANGAN
                </td>
            </tr>
            <?php $no=1; foreach($hadir as $value){?>
            <tr>
                <td>
                <?php echo $no?>
                </td>
                    
                <td>
                    <?php echo $value['0']?>
                </td>
                    
                <td>
                    <?php echo $value['1']?>
                </td>
                    
                <td>
                <?php echo $value['0']?>
                </td>
            </tr>
            <?php $no++; }?>
            </table>
            <br>
            <br>
            <br>
            <p class='font-10 bold justify'>Undangan Yang Tidak Hadir:</p>
            <table class='font-10 center'>
            <?php $no=1; foreach($tidak_hadir as $value){?>
            <tr>
                <td>
                <?php echo $no?>
                </td>
                    
                <td>
                    <?php echo $value['0']?>
                </td>
                    
                <td>
                    <?php echo $value['1']?>
                </td>
                    
                <td>
                <?php echo $value['0']?>
                </td>
            </tr>
            <?php $no++; }?>
            </table>

        
    </body>

</html>