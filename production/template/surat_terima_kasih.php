<html>

    <head>
        <title>Surat Terima Kasih</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-18{

                font-size:18pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            
            .ttd{
                float:right;
                width:50%; 

                
            }
        </style>
    </head>


    <body>

        <table>
            <tr>
                <td style='width:auto'>
                    Nomor
                </td>
                <td style='width:auto'>
                    :
                </td >
                <td>
                    <?php echo $row->no_surat?>
                </td>
            </tr>

            <tr>
                <td>
                    Lamp
                </td>
                <td>
                    :
                </td>
                <td>
                    <?php echo $row->lampiran?>
                </td>
            </tr>

             <tr>
                <td>
                    Hal
                </td>
                <td>
                    :
                </td>
                <td>
                    <?php echo $row->hal?>
                </td>
            </tr>
        </table>
        <br>
        <p class='font-10 justify'>Kepada Yth.</p>
        <p class='font-10 justify'><?php echo $row->kepada?></p>
        
        <p class='font-10 justify'><?php echo $row->dikota?></p>
        <br>
        <p class='font-10 justify'>Assalâmu’alaikum warahmatullâhi wabarakâtuh,</p>
        <p class='font-10 justify'>Dengan hormat, <?php echo $row->isi?></p>
        <table class='table'>
            <tr>
                <td style='width:100px'>
                    Hari, Tanggal
                </td> 
                <td style='width:10px'>
                    :
                </td>
                <td style='width:auto'>
                    <?php echo $row->hari_tanggal?>
                </td>
            </tr>
            <tr>
                <td>
                    Waktu
                </td> 
                <td>
                    :
                </td>
                <td>
                    <?php echo $row->waktu?>
                </td>
            </tr>
            <tr>
                <td>
                    Tema
                </td>
                <td>
                    :
                </td> 
                <td>
                    <?php echo $row->tema?>
                </td>
            </tr>
            <tr>
                <td>
                    Tempat
                </td>
                <td>
                    :
                </td> 
                <td>
                    <?php echo $row->tempat?>
                </td>
            </tr>
        </table>
        <p class='font-10 justify'>Demikian, atas perhatian dan perkenannya kami ucapkan terima kasih.<strong></strong></p>
        <p class='font-10 justify'>Wassalâmu ’alaikum warahmatullâhi wabarakâtuh.</p>
        <br>
        <br>
        
        <div class='ttd'>
        <p class='font-12 '><?php echo $row->kota ?>, <?php echo $row->tanggal_masehi ?></p>
       <p class='font-12 '>Mengetahui,</p>
        <p class='font-12 '>Direktur DPPAI,</p>
        <br>
        <br>
        <br>
        <p class='font-12 '>Aunur Rohim Faqih, Dr., S.H., M.Hum.</p>
        </div>

        

        
    </body>

</html>