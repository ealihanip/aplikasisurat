<html>

    <head>
        <title>Pengislaman</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-18{

                font-size:18pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            .logo {
            float: center;
            color: white;
            display: block;
            width: 30px;
            height: 30px;
            padding: 50px;
            margin: 5px;
            border: 1px solid blue; 
            background-color: blue;
            border-style: center; 
            }
            .fhoto {
            float: left;
            color: black;
            display: block;
            width: 50%;
            width: 90px;
            height: 90px;
            padding: 50px;
            margin: 5px;
            border: 1px solid black; 
            background-color: white; 
            }
            table {
                border-collapse: collapse;
                width:80%;
                margin:0 auto;
            }

            table th {
                text-align:justify;
                height: 10px;
                border-bottom: 1px solid #ddd;
                border: 1px solid white;
                padding: 5px;
                
            }

            table td {
                font-size:12pt;
                height: 10px;
                vertical-align: bottom;
                border-bottom: 20px solid white;
                border: 1px solid white;
                padding: 5px;
                
            }
            .ttd{
                float:right;
                width:50%; 
            }
        </style>
    </head>


    <body>
        <div class='logo'>
        <p class='font-10'></p>
        </div>
        <div style="color:blue">
        <p class='font-10 solid blue center'>UNIVERSITAS<br>DIREKTORAT PENDIDIKAN DAN PENGEMBANGAN AGAMA ISLAM<br>Gedung Masjid Ulil Albab Jl. Kaliurang Km. 14,5 Besi Kota Telp. [0123] 444444</p>
        </div>
        <p class='font-14 center'>FORMULIR PENGISLAMAN</p>
        <br>
        <table class='font-10'>
            <tr>
                <td style='width:250px'>
                    1. N a m a
                </td>
                <td>
                    :<?php echo $row->nama?>
                </td>
            </tr>
            <tr>
                <td>
                    2. Tempat Tanggal Lahir
                </td>
                <td>
                    :<?php echo $row->tempat_tanggal_lahir?>
                </td>
            </tr>
            <tr>
                <td>
                    3. S u k u
                </td>
                <td>
                    :<?php echo $row->suku?>
                </td>
            </tr>
            <tr>
                <td>
                    4. Jenis Kelamin
                </td>
                <td>
                    :<?php echo $row->jenis_kelamin?>
                </td>
            </tr>
            <tr>
                <td>
                    5. Status Keluarga
                </td>
                <td>
                    :<?php echo $row->status_keluarga?>
                </td>
            </tr>
            <tr>
                <td>
                    6. Jumlah Saudara
                </td>
                <td>
                    :a. Laki-laki <?php echo $row->jumlah_saudara_laki_laki?> org. Agama <?php echo $row->agama_saudara_laki_laki?>
                </td>
            </tr>
            <tr>
                <td>
                
                </td>
                <td>
                    :b. Wanita<?php echo $row->jumlah_saudara_wanita?>org. Agama<?php echo $row->agama_saudara_wanita?>
                </td>
            </tr>
            <tr>
                <td>
                    7. Warga Negara
                </td>
                <td>
                    :<?php echo $row->warga_negara?>
                </td>
            </tr>
            <tr>
                <td>
                    8. Alamat Asal
                </td>
                <td>
                    :<?php echo $row->alamat_asal?>Telp. <?php echo $row->telp_alamat_asal?>
                </td>
            </tr>
            <tr>
                <td>
                    9. Alamat Sekarang
                </td>
                <td>
                    :<?php echo $row->alamat_sekarang?>Telp. <?php echo $row->telp_alamat_sekarang?>
                </td>
            </tr>
            <tr>
                <td>
                    10. Pekerjaan
                </td>
                <td>
                    :<?php echo $row->pekerjaan?>
                </td>
            </tr>
            <tr>
                <td>
                    11. Agama Asal
                </td>
                <td>
                    :<?php echo $row->agama_asal?>
                </td>
            </tr>
            <tr>
                <td>
                    12. Pendidikan Formal
                </td>


                <td>
                    SD
                </td>
            </tr>

            <tr>

                <td>
                    
                </td>
                <td>
                    SD
                </td>
            </tr>
            <tr>
                <td>
                    13. Nama Bapak
                </td>
                <td>
                    :........................................................................................
                </td>
            </tr>
            <tr>
                <td>
                    14. Nama Ibu
                </td>
                <td>
                    :........................................................................................
                </td>
            </tr>
            <tr>
                <td>
                    6. Jumlah Anak
                </td>
                <td>
                    :a. Laki-laki..................org. Agama.................................
                </td>
            </tr>
            <tr>
                <td>
                
                </td>
                <td>
                    :b. Wanita.....................org. Agama.................................
                </td>
            </tr>
            <tr>
                <td>
                    Nama & Alamat Pembimbing
                </td>
                <td>
                    :........................................................................................
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    .........................................................................................
                </td>
            </tr>
        </table>
        <br>
        <br>

        <p class='font-10 justify'>Isian yang saya tulis adalah benar adanya dan apabila tidak benar saya bersedia dituntut dihadapan pejabat yang berwewenang.<strong></strong></p>
        <br>
        <br>
        <br>
        <div class='fhoto'>
        <p class='font-10 CENTER'>UPLOAD<br>PHOTO<br><br>2x3 = 1lbr<br>4x6 = 1lbr</p>
        </div>
        <div class='ttd'>
        <p class='font-10 justify'>Kota, ____________ </p>
        <p class='font-10 justify'>Yang Bersangkutan,</p>
        <br>
        <br>
        <br>
        <br>
        <p class='font-10 justify'>..........................</p>
        </div>

        

        
    </body>

</html>