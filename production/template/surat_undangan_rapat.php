<html>
<head>
<title>Surat Undangan Rapat</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-16{

                font-size:16pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            
            .table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            .table th {
                text-align:center;
                border: solid black;
            }

            .table td {
                border: solid black;
                font-size:11pt;
                
            }
            .ttd{
                float:right;
                width:50%; 

                
            }
            
            .ttd{
                float:right;
                width:50%; 
                
            }
        </style>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (table.psd) -->
<table id="Table_01" width="1281" height="672" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="14">
			<img src="template/images/surat_undangan_rapat_01.png" width="1280" height="20" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="20" alt=""></td>
	</tr>
	<tr>
		<td rowspan="8">
			<img src="template/images/surat_undangan_rapat_02.png" width="20" height="652" alt=""></td>
		<td colspan="4">
			<img src="template/images/surat_undangan_rapat_03.png" width="381" height="130" alt=""></td>
		<td rowspan="8">
			<img src="template/images/surat_undangan_rapat_04.png" width="9" height="652" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_05.png" width="450" height="130" alt=""></td>
		<td rowspan="8">
			<img src="template/images/surat_undangan_rapat_06.png" width="10" height="652" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_07.png" width="390" height="130" alt=""></td>
		<td rowspan="8">
			<img src="template/images/surat_undangan_rapat_08.png" width="20" height="652" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="130" alt=""></td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="template/images/surat_undangan_rapat_09.png" width="381" height="18" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_10.png" width="450" height="18" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_11.png" width="390" height="18" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="18" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_12.png" width="380" height="13" alt=""></td>
		<td rowspan="6">
			<img src="template/images/surat_undangan_rapat_13.png" width="1" height="504" alt=""></td>
		<td colspan="3" rowspan="2">
			<img src="template/images/surat_undangan_rapat_14.png" width="450" height="31" alt=""></td>
		<td colspan="3" rowspan="2">
			<img src="template/images/surat_undangan_rapat_15.png" width="390" height="31" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="13" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="template/images/surat_undangan_rapat_16.png" width="6" height="451" alt=""></td>
		<td rowspan="3">

			<p class='font-12'>No	:  237/Dir./10/DPPAI/IX/2018</p>

			<p class='font-12'>H a l	:  <strong>UNDANGAN RAPAT</strong></p>

		

			<p class='font-12'>Kepada</p>
			<p class='font-12'><?php echo $row->kepada?></p></p>

			

			<p class='font-12'>Assalamualaikum wr. wb.</p>

			<p class='font-12'>Mengharap kehadiran semua Bapak/Ibu/Sdra/Sdri  untuk mengikuti rapat koordinasi pada :</p>

			<p class='font-12 bold'>Hari/Tanggal 	: <?php echo $row->hari?>, <?php echo $row->tanggal?></p>
			<p class='font-12 bold'>Waktu		: Pukul <?php echo $row->waktu?>WIB</p>
			<p class='font-12 bold'>Tempat	: <?php echo $row->tempat?></p>
			<br>
			<p class='font-12'>Acara  akan dimulai tepat pada waktunya dan bagi yang berhalangan hadir harap konfirmasi ke ke nomor 0852 1234  1234.</p>
			<br>

			<p class='font-12'>Terima kasih atas perhatian dan kehadirannya</p>
			<br>

			<p class='font-12'>Wassalamualaikum wr. wb</p>
			<br>
			<p class='font-12 center'>Direktur DPPAI,</p>

			<br>
			<br>
			<p class='font-12 center'>Dr. Aunur Rohim Faqih, S.H., M.Hum</p>
			

			
		</td>
		<td rowspan="4">
			<img src="template/images/surat_undangan_rapat_18.png" width="5" height="451" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="18" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="template/images/surat_undangan_rapat_19.png" width="12" height="433" alt=""></td>
		<td>

			<?php $no=1; foreach($undangan as $value){?>
				
				<?php echo $value['no']?>.<?php echo $value['nama']?>

				<br>
			
			<?php $no++; }?>	
			
		</td>
		<td rowspan="3">
			<img src="template/images/surat_undangan_rapat_21.png" width="13" height="433" alt=""></td>
		<td rowspan="3">
			<img src="template/images/surat_undangan_rapat_22.png" width="11" height="433" alt=""></td>
		<td>
		<?php $no=1; foreach($agenda as $value){?>
				
				<?php echo $value['no']?>.<?php echo $value['nama']?>

				<br>
			
			<?php $no++; }?></td>
		<td rowspan="3">
			<img src="template/images/surat_undangan_rapat_24.png" width="13" height="433" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="422" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="template/images/surat_undangan_rapat_25.png" width="425" height="11" alt=""></td>
		<td rowspan="2">
			<img src="template/images/surat_undangan_rapat_26.png" width="366" height="11" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="4" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="template/images/surat_undangan_rapat_27.png" width="369" height="7" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="7" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_28.png" width="380" height="40" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_29.png" width="450" height="40" alt=""></td>
		<td colspan="3">
			<img src="template/images/surat_undangan_rapat_30.png" width="390" height="40" alt=""></td>
		<td>
			<img src="template/images/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
</table>
<!-- End ImageReady Slices -->
</body>
</html>