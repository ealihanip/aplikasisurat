<html>

    <head>
        <title>Surat Tugas</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-16{

                font-size:16pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            
            .table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            .table th {
                text-align:center;
                border: solid black;
            }

            .table td {
                border: solid black;
                font-size:11pt;
                
            }
            .ttd{
                float:right;
                width:50%; 

                
            }
            
            .ttd{
                float:right;
                width:50%; 
                
            }
        </style>
    </head>


    <body>
        <p class='font-14 bold underline center'>Surat Tugas</p>
        <p class='font-10 center'>No. : 002/ST-Rek/DPPAI/VIII/2018</p>
        <br>
        <p class='font-10 center'>Bismillahirrahmanirrahim</p>
        <p class='font-10 justify'>Pimpinan Universitas di Kota, dengan ini memberi tugas kepada Saudara yang namanya tersebut di bawah ini:</p>
        <br>
        <table style="width:100%;">

            <?php foreach($penanggung_jawab as $penanggung_jawab){?>
            <tr>
                <?php if($penanggung_jawab['no']==1){?>

                    <td style="width:150px">
                        Penanggung Jawab
                    </td>
                    <td style="width:auto">
                        :
                    </td>

                <?php }else{?>

                    <td>

                    </td>

                    <td>

                    </td>
                
                <?php }?>
                

                <td style="width:100px">
                    <?php echo $penanggung_jawab['jabatan']?>
                </td>

                <td style="width:auto">
                    :
                </td>
                
                <td colspan='2' style="width:auto">
                    <?php echo $penanggung_jawab['nama']?>
                </td>

                
            </tr>
            <?php }?>


            

            <?php foreach($pengarah as $pengarah){?>
            <tr>
                <?php if($pengarah['no']==1){?>

                    <td style="width:auto">
                        Pengarah
                    </td>
                    <td style="width:auto">
                        :
                    </td>

                <?php }else{?>

                    <td>

                    </td>

                    <td>

                    </td>
                
                <?php }?>
                

                <td style="width:auto">
                    <?php echo $pengarah['jabatan']?>
                </td>
                <td style="width:auto">
                    :
                </td>
                
                <td colspan='2' style="width:auto">
                    <?php echo $pengarah['nama']?>
                </td>

                
            </tr>
            <?php }?>
            <?php foreach($ketua as $ketua){?>
            <tr>
                <?php if($ketua['no']==1){?>

                    <td style="width:auto">
                        Ketua
                    </td>
                    
                    <td style="width:auto">
                        :
                    </td>

                <?php }else{?>

                    <td>

                    </td>

                    <td>

                    </td>
                
                <?php }?>
                

                <td colspan='3' style="width:auto">
                    <?php echo $ketua['nama']?>
                </td>
                
                <td style="width:auto">
                    (<?php echo $ketua['jabatan']?>)
                </td>
            </tr>
            <?php }?>

            <?php foreach($anggota as $anggota){?>
            <tr>
                <?php if($anggota['no']==1){?>

                    <td style="width:auto">
                        Anggota
                    </td>
                    <td style="width:auto">
                        :
                    </td>

                <?php }else{?>

                    <td>

                    </td>

                    <td>

                    </td>
                
                <?php }?>
                

                <td colspan='3' style="width:auto">
                <?php echo $anggota['no']?>. <?php echo $anggota['nama']?>
                </td>
                
                <td style="width:auto">
                    (<?php echo $anggota['jabatan']?>)
                </td>
            </tr>
            <?php }?>


            
        </table>
        <br>

        <p class='font-10 justify'>Tugas yang diberikan adalah <?php echo $row->tugas?> <strong></strong></p>
        <p class='font-10 justify'>Demikian agar dapat dilaksanakan sebagaimana mestinya.</p>
        <p class='font-10 center'>Billahittaufiq wal hidayah</p>
        
        <div class='ttd'>
        <p class='font-12 '><?php echo $row->kota ?>, <?php echo $row->tanggal_masehi ?></p>
       <p class='font-12 '>Mengetahui,</p>
        <p class='font-12 '>Direktur DPPAI,</p>
        <br>
        <br>
        <br>
        <p class='font-12 '>Aunur Rohim Faqih, Dr., S.H., M.Hum.</p>
        </div>

        

        
    </body>

</html>