<html>

    <head>
        <title>Surat Perintah Lembur</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-18{

                font-size:18pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            table th {
                text-align:justify;
                height: 10px;
                border-bottom: 1px solid #ddd;
                border: 1px solid white;
                padding: 15px;
                
            }

            table td {
                font-size:10pt;
                height: 10px;
                vertical-align: bottom;
                border-bottom: 20px solid white;
                border: 1px solid white;
                padding: 5px;  
            }
            .ttd{
                float:right;
                width:50%; 

                
            }
        </style>
    </head>


    <body>
        <p class='font-18 bold underline center'>Surat Perintah Lembur</p>
        <p class='font-10 center'>No. :  <?php echo $row->no_surat?></p>
        <br>
        <br>
        <p class='font-10 justify'>Direktur Direktorat Pendidikan dan Pengembangan Agama Islam Universitas di Kota, dengan ini memberi perintah lembur kepada saudara yang namanya tersebut di bawah ini:</p>
        <br>
        <br>
        
        <table class='table'>
            <tr>
                <td style='width:auto'>
                    NO
                </td>
                <td>
                    NAMA
                </td>
                
            </tr>

            <?php foreach($nama as $nama){?>
                
                <tr>
                    <td>
                        <?php echo $nama['no']?>.
                    </td>
                    <td>
                        <?php echo $nama['nama']?>
                    </td>
                </tr>

            <?php }?>
            
            
        </table>
        <br>
        <br>

        <p class='font-10 justify'>Untuk menyiapkan dan melaksanakan <?php echo $row->tugas?>.</p>
        <p class='font-10 justify'>Demikian, surat ini dibuat agar dapat dilaksanakan sebagaimana mestinya.</p>
        
        <div class='ttd'>
        <p class='font-10 '><?php echo $row->kota?>, <?php echo $row->tanggal_masehi?></p>
        <p class='font-10 '>Mengetahui,</p>
        <p class='font-10 '>Direktur,</p>
        <br>
        <br>
        <br>
        <p class='font-10 '>Dr. Aunur Rohim Faqih, S.H., M.Hum</p>
        </div>

        

        
    </body>

</html>