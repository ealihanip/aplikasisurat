<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Tugas <small><a href="tabel_surat_tugas.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
					
					if(isset($_POST['input'])){
            

            // penanggung_jawab
            $i=true;
            $penanggung_jawab='';
            foreach($_POST['penanggung_jawab'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $penanggung_jawab .=$value.'-';
                  
                  $i=false;
                }else{

                  $penanggung_jawab .=$value.'&';

                  $i=true;
                }

              }
            }

            $penanggung_jawab=substr($penanggung_jawab,0,strlen($penanggung_jawab)-1);

            // end penanggung jawab

            // pengarah
            $i=true;
            $pengarah='';
            foreach($_POST['pengarah'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $pengarah .=$value.'-';
                  
                  $i=false;
                }else{

                  $pengarah .=$value.'&';

                  $i=true;
                }

              }
            }

            $pengarah=substr($pengarah,0,strlen($pengarah)-1);

          
            // end pengarah

            // ketua
            $i=true;
            $ketua='';
            foreach($_POST['ketua'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $ketua .=$value.'-';
                  
                  $i=false;
                }else{

                  $ketua .=$value.'&';

                  $i=true;
                }

              }
            }

            $ketua=substr($ketua,0,strlen($ketua)-1);

          
            // end ketua


            // anggota
            $i=true;
            $anggota='';
            foreach($_POST['anggota'] as $value){

              if(!empty($value)){

                if($i==true){
                  
                  $anggota .=$value.'-';
                  
                  $i=false;
                }else{

                  $anggota .=$value.'&';

                  $i=true;
                }

              }
            }

            $anggota=substr($anggota,0,strlen($anggota)-1);

          
            // end anggota
            

            // mencari no surat
            $query = "SELECT max(no_surat) as maxKode FROM surat_tugas";
            $hasil = mysqli_query($link,$query);
            $data = mysqli_fetch_array($hasil);
            $kode = $data['maxKode'];
          
            if(!empty($kode)){

              $noUrut = (int) substr($kode, 0, 3);
            
              $noUrut++;
              
              $char = "/Dir/10/DPPAI/IX/2018  ";
              $kode = sprintf("%03s", $noUrut).$char;
              

            }else{

              $kode = '001/Dir/10/DPPAI/IX/2018';
            }
          
            
            $// mencari no surat
           $query = "SELECT max(no_surat) as maxKode FROM surat_tugas";
           $hasil = mysqli_query($link,$query);
           $data = mysqli_fetch_array($hasil);
           $kode = $data['maxKode'];
         
           if(!empty($kode)){

             $noUrut = (int) substr($kode, 0, 3);
           
             $noUrut++;
             
             $char = "/Dir/10/DPPAI/IX/2018  ";
             $kode = sprintf("%03s", $noUrut).$char;
             

           }else{

             $kode = '001/Dir/10/DPPAI/IX/2018';
           }
         
           
           $no_surat=$kode;

            $date=date('Y-m-d');
            
						$up = mysqli_query($link, "INSERT INTO surat_tugas VALUES 
							('',
            	'$no_surat',
            	'$penanggung_jawab',
            	'$pengarah',
            	'$ketua',
              '$anggota',
            	'$_POST[tugas]',
            	'$_POST[kota]',
            	'$_POST[mengetahui]',
              '$_POST[namamengetahui]',
							'$_POST[tanggal_m]',
							'$date'
							)
						
						");
						
						if($up){
              
              
              header("Location: form_surat_tugas.php?ID=".$_POST['no_surat']."&&sukses=ya");
             
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Diarsipkan !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_surat_tugas.php');
								  } ,1000); 
								</script>";
					}
					
					?>
                    <form method="post" name="surat_tugas" id="surat_tugas" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">

                      

                      <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penanggung Jawab<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-penanggung_jawab">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="penanggung_jawab[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="penanggung_jawab[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-penanggung_jawab" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-penanggung_jawab hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="penanggung_jawab[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="penanggung_jawab[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-penanggung_jawab").click(function(){ 
                              var html = $(".copy-penanggung_jawab").html();
                              $(".after-penanggung_jawab").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->

                    <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pengarah<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-pengarah">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="pengarah[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="pengarah[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-pengarah" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-pengarah hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="pengarah[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="pengarah[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-pengarah").click(function(){ 
                              var html = $(".copy-pengarah").html();
                              $(".after-pengarah").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                      </script>


                    <!-- end add more -->

                    <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ketua<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-ketua">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="ketua[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="ketua[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-ketua" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-ketua hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="ketua[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="ketua[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-ketua").click(function(){ 
                              var html = $(".copy-ketua").html();
                              $(".after-ketua").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->


                    <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Aanggota<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-anggota">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="anggota[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="anggota[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-anggota" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-anggota hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-6'>
                            <input type="text" name="anggota[]" class="form-control" placeholder="Jabatan">
                            </div>

                            <div class='col-lg-6'>
                            <input type="text" name="anggota[]" class="form-control" placeholder="Nama">
                            </div>
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-anggota").click(function(){ 
                              var html = $(".copy-anggota").html();
                              $(".after-anggota").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->


                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tugas <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control" rows="5" name="tugas"></textarea>
                        </div>
                      </div>

                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kota" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mengetahui <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="mengetahui" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="namamengetahui" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

									

											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Masehi <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="tanggal_m" required="required" class="date-picker form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Atur Ulang</button>
                          <button name='input' type="submit" class="btn btn-success">Buat Surat</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>