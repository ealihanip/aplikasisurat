<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Surat Undangan Rapat <small><a href="tabel_surat_undangan_rapat.php">lihat Data</a></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
					<?php
					require_once ("function.php");	
					
					if(isset($_POST['input'])){
            

             // undangan
           
             $undangan='';
             foreach($_POST['undangan'] as $value){
 
               if(!empty($value)){
 
               
 
                 $undangan .=$value.'&';
 
                 
 
               }
             }
 
             $undangan=substr($undangan,0,strlen($undangan)-1);
 
             // end undangan

             // agenda
           
             $agenda='';
             foreach($_POST['agenda'] as $value){
 
               if(!empty($value)){
 
               
 
                 $agenda .=$value.'&';
 
                 
 
               }
             }
 
             $agenda=substr($agenda,0,strlen($agenda)-1);
 
             // end agenda

            

            $date=date('Y-m-d');
            
            print_r($agenda);

            // mencari no surat
            $query = "SELECT max(no_surat) as maxKode FROM surat_undangan_rapat";
            $hasil = mysqli_query($link,$query);
            $data = mysqli_fetch_array($hasil);
            $kode = $data['maxKode'];
          
            if(!empty($kode)){

              $noUrut = (int) substr($kode, 0, 3);
            
              $noUrut++;
              
              $char = "/Dir./10/DPPAI/IX/2018";
              $kode = sprintf("%03s", $noUrut).$char;
              

            }else{

              $kode = '001/Dir./10/DPPAI/IX/2018';
            }
          
            
            $no_surat=$kode;

            //selesai cari no surat

						$up = mysqli_query($link, "INSERT INTO surat_undangan_rapat VALUES 
							('',
            	'$no_surat',
            	'$_POST[kepada]',
              '$_POST[hari]',
              '$_POST[tanggal]',
            	'$_POST[waktu]',
							'$_POST[tempat]',
							'$undangan',
              '$agenda',
              '$date'
							)
						
						");
						
						if($up){
              
              
              header("Location: form_surat_undangan_rapat.php?ID=".$_POST['no_surat']."&&sukses=ya");
             
							
						}else{
							
							
						}
					}
				

					
					if(isset($_GET['sukses']) == 'ya'){
						echo "<script type='text/javascript'>
									setTimeout(function () {  
										swal({
										title: 'Surat Berhasil Diarsipkan !!',
										type: 'success',
										timer: 3000,
										showConfirmButton: true
									   });  
									},10); 
								  window.setTimeout(function(){ 
								   window.location.replace('tabel_surat_undangan_rapat.php');
								  } ,1000); 
								</script>";
					}
					
					?>
                    <form method="post" name="surat_undangan_rapat" id="surat_undangan_rapat" enctype="multipart/form-data" action="" data-parsley-validate class="form-horizontal form-label-left">
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kepada" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">hari <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="hari" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hari<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="tanggal" required="required" class="date-picker form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Waktu <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="waktu" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tempat <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="tempat" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                     

                      <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Undangan<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-undangan">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="undangan[]" class="form-control" placeholder="Nama">
                            </div>

                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-undangan" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-undangan hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="undangan[]" class="form-control" placeholder="Nama">
                            </div>

                           
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-undangan").click(function(){ 
                              var html = $(".copy-undangan").html();
                              $(".after-undangan").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->


                    <!-- pake add more -->

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Agenda<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group control-group after-agenda">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="agenda[]" class="form-control" placeholder="Agenda">
                            </div>

                        </div>
                        
                        <div class="input-group-btn"> 
                          <button class="btn btn-success add-more-agenda" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                        </div>
                      </div>
                        </div>
                      </div>


                     
                      <div class="copy-agenda hide">
                        <div class="control-group input-group" style="margin-top:10px">
                        <div class='row'>
                            <div class='col-lg-12'>
                            <input type="text" name="agenda[]" class="form-control" placeholder="Agenda">
                            </div>

                           
                        </div>
                          <div class="input-group-btn"> 
                            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                          </div>
                        </div>
                      </div>


                      <script type="text/javascript">
                        $(document).ready(function() {
                          $(".add-more-agenda").click(function(){ 
                              var html = $(".copy-agenda").html();
                              $(".after-agenda").after(html);
                          });
                          $("body").on("click",".remove",function(){ 
                              $(this).parents(".control-group").remove();
                          });
                        });
                    </script>


                    <!-- end add more -->

                    
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Atur Ulang</button>
                          <button name='input' type="submit" class="btn btn-success">Buat Surat</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>