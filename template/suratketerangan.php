<html>

    <head>
        <title>Surat Keterangan</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-16{

                font-size:16pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            
            .table {
                border-collapse: collapse;
                width:90%;
                margin:0 auto;
            }

            .table th {
                text-align:center;
                border: 1px solid black;
            }

            .table td {
                border: 1px solid black;
                font-size:12pt;
                
            }
            .ttd{
                float:right;
                width:50%; 
                
            }
        </style>
    </head>


    <body>
        <p class='font-16 bold underline center'>Surat Keterangan</p>
        <p class='font-12 center'>No. : 254/Dir/10/DPPAI/IX/2018</p>
        <br>
        <br>
        <p class='font-12 justify'>Direktur Direktorat Pendidikan dan Pengembangan Agama Islam (DPPAI) Universitas menerangkan bahwa Saudara/i yang namanya tersebut berikut ini:</p>
        <br>
        <br>
        <table class='table'>
            <tr>
                <th>
                    NIM
                </th>
                <th>
                    NAMA
                </th>
                <th>
                    Jurusan
                </th>
                <th>
                    Fakultas
                </th>
            </tr>

            <tr>
                <td>
                15711129
                </td>
                <td>
                NESTI HERENNADIA
                </td>
                <td>
                Pend. Kedokteran
                </td>
                <td>
                Kedokteran
                </td>
            </tr>
        </table>
        <br>
        <br>

        <p class='font-12 justify'>Benar-benar telah mengikuti <strong>Ujian BTAQ, Kamis/ 13 September 2018</strong> dan dinyatakan Lulus dengan nilai <strong>C</strong></p>
        <p class='font-12 justify'>Demikian surat keterangan ini dibuat agar dapat dipergunakan sebagaimana mestinya.</p>
        

        <div class='ttd'>
        <p class='font-12 '>Kota, 04 Muharram 1440  H</p>
        <p class='font-12 '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14 September 2018 M</p>
        <p class='font-12 '>Mengetahui,</p>
        <p class='font-12 '>Direktur DPPAI,</p>
        <br>
        <br>
        <br>
        <p class='font-12 '>Aunur Rohim Faqih, Dr., S.H., M.Hum.</p>
        </div>

        

        
    </body>

</html>